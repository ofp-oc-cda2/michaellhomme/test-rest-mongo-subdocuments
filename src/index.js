require('dotenv').config()

const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const { expressErrorHandler } = require('./controllers/Errors')
const { setupUserController } = require('./controllers/UserController')
const { setupClassController } = require('./controllers/ClassController')
const { setupUsersInClassController } = require('./controllers/UsersInClassController')

const HTTP_PORT = 8080

// Configure express
const app = express()
app.use(cors())
app.use(express.json())

// Setup mongoose
mongoose.connect('mongodb://mongo:27017/test', { authSource: 'admin', user: 'root', pass: 'example' })
mongoose.connection.on('error', console.error.bind(console, 'error connect mongoose'))
mongoose.connection.once('open', () => console.log('Connected to MongoDB'))


// Hello
app.get('/', (req, res) => res.send('Hello World!'))

// Register controllers
setupUserController(app)
setupClassController(app)
setupUsersInClassController(app)

// Register error handler
app.use(expressErrorHandler)

// Start web server
app.listen(HTTP_PORT, () => console.log(`HTTP server listening on port ${HTTP_PORT}`))
