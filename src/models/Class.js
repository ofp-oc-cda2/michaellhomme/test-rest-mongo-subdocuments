const mongoose = require('mongoose')
const { userSchema } = require('./User')

const classSchema = new mongoose.Schema({
  name: String,
  users: [userSchema]
})

const Class = mongoose.model('Class', classSchema)

module.exports = {
  Class,
  classSchema
}
