// Base class for HTTP errors, used by expressErrorHandler
class HTTPError extends Error {
  constructor() {
    super()
    this.message = ''
    this.statusCode = 500
  }
}


// Error when an ID is missing
class MissingId extends HTTPError {
  constructor() {
    super()
    this.name = this.constructor.name
    this.message = 'Missing entity ID in request path'
    this.statusCode = 422
  }
}


// Error when a parameter is missing
class MissingParameter extends HTTPError {
  constructor(parameterName) {
    super()
    this.name = this.constructor.name
    this.message = `Missing parameter '${parameterName}' in request body`
    this.statusCode = 422
  }
}


// HTTP 404
class NotFound extends HTTPError {
  constructor(parameterName) {
    super()
    this.name = this.constructor.name
    this.message = 'Entity not found'
    this.statusCode = 404
  }
}


// Express error handler for HTTP errors
const expressErrorHandler = (err, req, res, next) => {
  console.log('toto')
  if (err instanceof HTTPError) {
  console.log('titi')
    res.status(err.statusCode).send(err.message)
  } else {
    console.log(err)
    next(err)
  }
}

module.exports = {
  HTTPError,
  MissingId,
  MissingParameter,
  NotFound,
  expressErrorHandler
}
