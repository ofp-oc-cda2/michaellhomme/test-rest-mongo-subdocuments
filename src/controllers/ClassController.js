const { Class } = require('../models/Class')

const { NotFound } = require('./Errors')
const { getId, getParam } = require('./Controller')


// List classes
const listClasses = (req, res, next) => {
  Class.find({})
    .then(classes => res.send(classes))
    .catch(error => next(error))
}

// Create class
const createClass = (req, res, next) => {
  const name = getParam(req, 'name')

  Class.create({ name })
    .then(aClass => res.status(200).send(aClass))
    .catch(error => next(error))
}

// Update user
const updateClass = (req, res, next) => {
  const classId = getId(req)
  const newName = getParam(req, 'name')

  Class.findByIdAndUpdate(
    { _id: classId },
    { $set: {
      name: newName
    } },
    { returnDocument: 'after' }
  )
    .then(aClass => {
      if(aClass === null) next(new NotFound())
      else res.send(aClass)
    })
    .catch(error => next(error))
}

// Get user
const getClass = (req, res, next) => {
  const classId = getId(req)

  Class.findOne({ _id: classId })
    .then(aClass => {
      if(aClass === null) next(new NotFound())
      else res.send(aClass)
    })
    .catch(error => next(error))
}

// Delete user
const deleteClass = (req, res, next) => {
  const classId = getId(req)

  Class.findByIdAndDelete(classId)
    .then(result => {
      if(result === null) next(new NotFound())
      else res.sendStatus(200)
    })
    .catch(error => next(error))
}


// Setup controller
function setupClassController(express) {
  express.get('/classes', listClasses)
  express.post('/classes/', createClass)
  express.get('/classes/:id', getClass)
  express.put('/classes/:id', updateClass)
  express.delete('/classes/:id', deleteClass)
}

module.exports = {
  setupClassController
}
