const { User } = require('../models/User')
const { Class } = require('../models/Class')

const { NotFound } = require('./Errors')
const { getId, getParam } = require('./Controller')


// Helper function to retrieve a class by its ID
// Return a promise resolved with a class (never null), or rejected with an error
function getClass(req) {
  return new Promise((resolve, reject) => {
    Class.findOne({ _id: getId(req, 'classId') })
      .then(aClass => {
        if(aClass === null) reject(new NotFound())
        else resolve(aClass)
      })
      .catch(error => reject(error))
  })
}


// List users in class
const listUsers = (req, res) => {
  getClass(req)
    .then(aClass => res.send(aClass.users))
    .catch(error => next(error))
}

// Create user in class
const createUser = (req, res, next) => {
  const username = getParam(req, 'username')

  getClass(req)
    .then(aClass => {
      const user = aClass.users.create({ username })
      aClass.users.push(user)
      aClass.save()
        .then(() => res.send(user))
        .catch(error => next(error))
    })
    .catch(error => next(error))
}

// Update user
const updateUser = (req, res, next) => {
  const userId = getId(req)
  const newUsername = getParam(req, 'username')

  getClass(req)
    .then(aClass => {
      const user = aClass.users.id(userId)
      if(!user) next(new NotFound())
      else {
        user.username = newUsername
        aClass.save()
          .then(() => res.send(user))
          .catch(error => next(error))
      }
    })
    .catch(error => next(error))
}

// Get user in class
const getUser = (req, res, next) => {
  const userId = getId(req)

  getClass(req)
    .then(aClass => {
      const user = aClass.users.id(userId)
      if(user) res.send(user)
      else next(new NotFound())
    })
    .catch(error => next(error))
}

// Delete user
const deleteUser = (req, res, next) => {
  const userId = getId(req)

  getClass(req)
    .then(aClass => {
      const user = aClass.users.id(userId)
      if(!user) next(new NotFound())
      else {
        user.remove()
        aClass.save()
          .then(() => res.send())
          .catch(error => next(error))
      }
    })
    .catch(error => next(error))
}


// Setup controller
function setupUsersInClassController(express) {
  express.get('/classes/:classId/users', listUsers)
  express.post('/classes/:classId/users/', createUser)
  express.get('/classes/:classId/users/:id', getUser)
  express.put('/classes/:classId/users/:id', updateUser)
  express.delete('/classes/:classId/users/:id', deleteUser)
}

module.exports = {
  setupUsersInClassController
}
