const { User } = require('../models/User')

const { NotFound } = require('./Errors')
const { getId, getParam } = require('./Controller')


// List users
const listUsers = (req, res) => {
  User.find({})
    .then(users => res.send(users))
    .catch(error => next(error))
}

// Create user
const createUser = (req, res, next) => {
  const username = getParam(req, 'username')

  User.create({ username })
    .then(user => res.status(200).send(user))
    .catch(error => next(error))
}

// Update user
const updateUser = (req, res, next) => {
  const userId = getId(req)
  const newUsername = getParam(req, 'username')

  User.findByIdAndUpdate(
    { _id: userId },
    { $set: {
      username: newUsername
    } },
    { returnDocument: 'after' }
  )
    .then(user => {
      if(user === null) next(new NotFound())
      else res.send(user)
    })
    .catch(error => next(error))
}

// Get user
const getUser = (req, res, next) => {
  const userId = getId(req)

  User.findOne({ _id: userId })
    .then(user => {
      if(user === null) next(new NotFound())
      else res.send(user)
    })
    .catch(error => next(error))
}

// Delete user
const deleteUser = (req, res, next) => {
  const userId = getId(req)

  User.findByIdAndDelete(userId)
    .then(result => {
      if(result === null) next(new NotFound())
      else res.sendStatus(200)
    })
    .catch(error => next(error))
}


// Setup controller
function setupUserController(express) {
  express.get('/users', listUsers)
  express.post('/users/', createUser)
  express.get('/users/:id', getUser)
  express.put('/users/:id', updateUser)
  express.delete('/users/:id', deleteUser)
}

module.exports = {
  setupUserController
}
