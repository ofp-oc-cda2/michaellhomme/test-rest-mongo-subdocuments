const ObjectID = require('mongodb').ObjectId

const { MissingId, MissingParameter } = require('./Errors')

// Helper to retrieve a Mongoose ID from the request path
// Generate an error if the ID is missing
function getId(req, optionalIdName) {
  const field = optionalIdName ? optionalIdName : 'id'

  if(!req.params[field]) throw new MissingId()
  return ObjectID(req.params[field])
}

// Helper to retrieve a value from the request body
// Generate an error if the parameter is missing
function getParam(req, parameterName) {
  if(!req.body[parameterName]) throw new MissingParameter(parameterName)
  return req.body[parameterName]
}


module.exports = {
  getId,
  getParam
}
